use clap::Parser;
use std::fs::{read_to_string, File};
use std::path::{Path, PathBuf};
use xmltree::{Element, XMLNode};

#[derive(Parser)]
struct CliArgs {
    #[arg(short, long)]
    filename: PathBuf,
    #[arg(short, long)]
    destination: PathBuf,
    #[arg(long)]
    file_prefix: Option<String>,
}

fn main() {
    let args = CliArgs::parse();

    let coverage_element = read_xml_file(&args.filename);
    let file_prefix = args.file_prefix.unwrap_or_else(|| "cobertura".into());
    split_file_by_packages(coverage_element, &args.destination, &file_prefix);
}

fn read_xml_file(filename: &PathBuf) -> Element {
    let data = read_to_string(filename).expect("Failed to read file");

    Element::parse(data.as_bytes()).unwrap()
}

fn split_file_by_packages(mut coverage_element: Element, destination: &Path, file_prefix: &str) {
    let packages = coverage_element
        .take_child("packages")
        .expect("Can't find packages element");

    for (i, package) in packages.children.into_iter().enumerate() {
        let mut partial_coverage_element = coverage_element.clone();
        let mut new_packages = Element::new("packages");
        new_packages.children.push(package);
        partial_coverage_element
            .children
            .push(XMLNode::Element(new_packages));
        write_to_destination(partial_coverage_element, destination, file_prefix, i);
    }
}

fn write_to_destination(element: Element, destination: &Path, file_prefix: &str, idx: usize) {
    let filename = format!("{}/{file_prefix}-{idx}.xml", destination.display());
    match element.write(File::create(&filename).unwrap()) {
        Ok(_) => println!("created file: {:?}", filename),
        Err(e) => println!("Failed to create file: {:?}", e),
    }
}
