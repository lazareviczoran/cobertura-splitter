## cobertura-splitter

A small cli that splits up a cobertura xml file into multiple files per package.
This is a workaround for the 10MB [gitlab pipeline artifact limit](https://gitlab.com/gitlab-org/gitlab/-/issues/328772).
